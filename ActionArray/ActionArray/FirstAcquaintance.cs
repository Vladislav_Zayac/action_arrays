﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray
{
    public class FirstAcquaintance
    {
        public void Run()
        {
            // ДЗ 1 по C#
            int[] array_first = new int[] { 1, 5, 10, -5, 2, 0 };   //объявляем массив
            Console.Write("Исходный массив: ");
            foreach (int number in array_first)                      //перебираем все элементы массива и выводим в консоль
            {
                Console.Write($"{number}|");
            }
            int sum = 0;                                            //объявляем сумму 
            foreach(int number in array_first)                      //перебираем все элементы массива и высчитываем сумму всех элементов
            {
                sum += number;                                      
            }
            Console.WriteLine();
            Console.WriteLine($"Сумма массива ровна {sum}.");       //выводим сумму в консоль
            Console.Write("Массив, в котором все элементы увеличены на 5: ");
            for (int i=0; i<array_first.Length;i++)                  //перебираем массив и каждый элемент увеличиваем на 5
            {
                array_first[i] += 5;
                Console.Write(array_first[i] + "|");                //выводим массив в консоль
            }
            Console.WriteLine();
            Console.Write("Массив, в котором все элементы уменьшены в 2 раза: ");
            array_first = new int[] { 1, 5, 10, -5, 2, 0 };         //присваеваем массиву исходные значения
            float[] array_second = new float[6];                    //объявляем второй массив
            for (int i = 0; i < array_first.Length; i++)            //перебираем массив, уменьшаем каждый элемент в 2 раза с конверктацией в тип float и выводим в консоль изменненый массив
            {
                array_second[i]=(float)array_first[i] / 2;         
                Console.Write(array_second[i] + "|");
            }
            try
            {

                int n = Convert.ToInt32(Console.ReadLine());
                switch (n)
                {
                    case 1:
                        Console.WriteLine("Вы ввели 1");
                        break;
                    case 5:
                        Console.WriteLine("Вы ввели 5");
                        break;
                    default:
                        Console.WriteLine("no");
                        break;
                }
            }
            catch
            {
                Console.WriteLine("Ошибка! Вы ввели не число!");
            }


            string str = Console.ReadLine();
            int count = 0;
            foreach (char j in str.ToLower())
                if (j == 'a')
                    count += 1;
            Console.WriteLine($"Количество буквы а в строке равно {count}");

            int[] mas = new int[] { 2, 4, 3, 12, -5, 23, 53 };
            int maxnumber=mas[0];
            foreach (int num in mas)
                if (num > maxnumber)
                    maxnumber = num;
            Console.WriteLine($"максимальное число в строке {maxnumber}");

            string str2 = Console.ReadLine();
            char ch3 = str2[0];
            foreach(char ch in str2)
                if(ch3>ch)
                    ch3 = ch;
            Console.WriteLine(ch3);            
        }
    }
}
