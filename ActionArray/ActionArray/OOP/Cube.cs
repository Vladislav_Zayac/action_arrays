﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray.OOP
{
    public class Cube:Figure
    {
        protected int s1;
        protected int s2;
        public Cube():base()
        {
            s1 = 3;
            s2 = 4;
        }
        public Cube(int s1, int s2) : base()
        {
            this.s1 = s1;
            this.s2 = s2;
        }
        public override void Perimeter()
        {
            int per = s1 + s2;
            Console.WriteLine($"Периметр прямоугольника - {per}");
        }
        public override void Square()
        {
            int sqr = s1 * s2;
            Console.WriteLine($"Площадь прямоугольника - {sqr}");
        }
    }
}
