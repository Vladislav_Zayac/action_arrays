﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray.OOP
{
    public class Circle:Figure
    {
        protected int radius;
        public Circle():base()
        {
            radius = 15;
        }
        public Circle(int radius) : base()
        {
            this.radius = radius;
        }
        public override void Square()
        {
            double sqr = Math.PI * Math.Pow(radius,2);
            Console.WriteLine($"Площадь круга - {sqr}");
        }
        public override void Perimeter()
        {
            double per = 2 * Math.PI * radius;
            Console.WriteLine($"Периметр круга - {per}");
        }
    }
}
