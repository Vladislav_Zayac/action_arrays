﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray
{
    public class Prog
    {
        public void ProgLesson()
        {
            Lesson10 lesson10 = new Lesson10();
            lesson10.Sound();
            Dog a = new Dog();
            a.Sound();
            Cat cat = new Cat();
            cat.Sound();
            Lesson10 q = new Cat();
            q.Sound();
            Lesson10 animal = a;
            animal.Sound();
        }
    }
    public class Lesson10
    {
        private string name;
        public Lesson10()
        {
            
            this.name = "baze";
        }
        public Lesson10(string name)
        {
            this.name = name;
        }
        public virtual void Sound()
        {
            Console.WriteLine("Sound");
        }
        public void WriteName()
        {
            Console.WriteLine(name);
        }
    }
    public class Dog: Lesson10
    {
        protected int wrigth;
        public Dog() : base()
        {
            wrigth = 1;
        }
        public Dog(string name, int weugth) : base(name)
        {
            this.wrigth = weugth;
        }
        public override void Sound()
        {
            Console.WriteLine("Gav gav");
        }
    }
    public class Cat: Lesson10
    {
        public Cat()
        {

        }
        public override void Sound()
        {
            Console.WriteLine("mew mew");
        }
    }
}
