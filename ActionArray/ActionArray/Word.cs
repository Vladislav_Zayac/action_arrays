﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray
{
    public class Word
    {
        private string SecretWord;
        private char[] Winword;

        public Word()
        {
            SecretWord = RandomWord();
            Winword = new char[SecretWord.Length];
            for (int i = 0; i < Winword.Length; i++)
            {
                Winword[i] = '*';
            }
        }
        public bool CheckAllWord(string str)
        {
            if (str == SecretWord)
            {
                Winword = SecretWord.ToCharArray();
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public string RandomWord()
        {
            string puth = "LibraryWords.txt";
            Random rnd = new();
            int rndWord = rnd.Next(0, File.ReadAllLines(puth).Length - 1);
            string secretWord = File.ReadLines(puth).Skip(rndWord).First();
            return secretWord;
        }
        public string GetWinword()
        {
            string s = "";
            foreach (char item in Winword)
                s += item;
            return s;
        }
        public string GetSecretWord()
        {
            return SecretWord.ToString();
        }
        public bool CheckLetter(char letter)
        {
            if (SecretWord.Contains(letter))
            {
                for (int i = 0; i < SecretWord.Length; i++)
                {
                    if (SecretWord[i] == letter)
                    {
                        Winword[i] = letter;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
