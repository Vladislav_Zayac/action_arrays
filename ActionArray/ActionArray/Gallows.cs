﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ActionArray
{
    public class Gallows
    {
        public void Start()
        {
            Console.WriteLine("-----------------------ИГРА НАЧАЛАСЬ------------------");
            Console.WriteLine("Привет, Игрок! " +
                "\nДобро пожаловать в игру Виселица!" +
                "\nПравила игры:" +
                "\nТебе нужно угадать слово по буквам. Количество жизней равно 6." +
                "\nУдачи!");

            Word word = new Word();
            int health = 6;
            while(health>0 && word.GetWinword().Contains('*'))
            {
                Console.WriteLine("Если вы знаете слово, то нажмите 'y' для ввода слова целиком. Если нет, то нажмите 'Enter'.");
                string full = Console.ReadLine();
                Console.WriteLine($"Загаднное слово: {word.GetWinword()}");
                if (full == "у")
                {
                    Console.WriteLine("Введите слово целиком");
                    bool result = word.CheckAllWord(Console.ReadLine());
                    if (result)
                    {
                        Console.WriteLine("Слово верно!");
                    }
                    else
                    {
                        Console.WriteLine("Слово не верно!");
                        health = 0;
                    }
                }
                else
                {
                    Console.WriteLine("Введите букву");
                    Console.Write("Буква: ");
                    string letter_string = Console.ReadLine();
                    char letter = letter_string[0];
                    bool result1 = word.CheckLetter(letter);
                    if (result1)
                    {
                        Console.WriteLine("Вы угадали букву!");
                    }
                    else
                    {
                        health -= 1;
                        Console.WriteLine("Такой буквы нет!");
                        Console.WriteLine($"У вас осталось {health} жизни!");
                    }
                }
            }            
            if (!word.GetWinword().Contains('*')) 
            { 
                Console.WriteLine("Ты победил!");
                Console.WriteLine($"Загаданное слово было {word.GetSecretWord()}!");
            }
            else
            {
                Console.WriteLine("Ты проиграл!");
                Console.WriteLine($"Загаданное слово было {word.GetSecretWord()}!");
            }
            Console.WriteLine("-----------------------ИГРА ОКОНЧЕНА------------------");
            Console.ReadKey();
        }
    }
}
