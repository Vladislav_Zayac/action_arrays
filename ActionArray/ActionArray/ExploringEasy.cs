﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray
{
    public class ExploringEasy
    {
        string[] tasks = new string[] { "Задача 1: написать программу вычисления суммы и произведения двух чисел", "Задача 2: написать программу вычисления площади прямоугольника",
                "Задача 3: написать программу вычисления площади круга по радиусу", "Задача 4: написать программу вычисления среднего арифметического трех чисел",
                                "Задача 5: Дано число, если оно больше 10, то уменьшить его на 10, если меньше то увеличить на 10.",
                                "Задача 6: Дано два числа. Если их сумма больше 100, вывести \"yes\", если меньше \"no\"",
                                "Задача 7: Дано число. Если оно четное - вывести \"yes\", если нечетное вывести \"no\"",
                                "Задача 8: Дан массив чисел. Найти среднее арифметическое его элементов",
                                "Задача 9: Создать массив, элементы которого равны квадрату своего индекса",
                                "Задача 10: Создать и заполнить массив четными числами начиная с 2",
                                "Задача 11: Дан массив чисел, подсчитать количество четных элементов в массиве",
                                "Задача 12: Дан массив чисел. Отсортировать массив по возрастанию. (Реализовать сортировку пузырьком)",
                                "Задача 13: Дан массив чисел. Найти все нечетные элементы и вывести их по убыванию",
                                "Задача 14: Дан массив чисел. Определить, есть ли в массиве повторяющиеся элементы",
                                "Задача 15: Дана строка. Определить количество повторений каждой буквы в этом слове. Пример: ввод - \"водолаз\", вывод - в - 1, о - 2, д - 1, л - 1, a - 1, з - 1"};
        public void Run()
        {
            ConsoleKeyInfo cki;
            do
            {
                Console.Clear();
                Console.WriteLine("Решатор 2000 приветствует Вас!");
                foreach(string s in tasks)
                    Console.WriteLine(s);
                Console.WriteLine("Выберите задачу, которую решить.");
                string numberTask = Console.ReadLine();
                switch (numberTask)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите число a: ");
                        int a = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Введите число b: ");
                        int b = Convert.ToInt32(Console.ReadLine());
                        SumAndMultiplication(a, b);
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите сторону side1: ");
                        float side1 = Convert.ToSingle(Console.ReadLine());
                        Console.Write("Введите сторону side2: ");
                        float side2 = Convert.ToSingle(Console.ReadLine());
                        AreaRectangle(side1, side2);
                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите радиус круга: ");
                        float circle = Convert.ToSingle(Console.ReadLine());
                        AreaCircle(circle);
                        break;
                    case "4":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите число a: ");
                        float numberA = Convert.ToSingle(Console.ReadLine());
                        Console.Write("Введите число b: ");
                        float numberB = Convert.ToSingle(Console.ReadLine());
                        Console.Write("Введите число c: ");
                        float numberC = Convert.ToSingle(Console.ReadLine());
                        ArithmeticMean(numberA, numberB, numberC);
                        break;
                    case "5":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите число: ");
                        int numberT = Convert.ToInt32(Console.ReadLine());
                        ProcessingNumber(numberT);
                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите число а: ");
                        int number1 = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Введите число b: ");
                        int number2 = Convert.ToInt32(Console.ReadLine());
                        ProcessingNumbers(number1, number2);
                        break;
                    case "7":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.Write("Введите число: ");
                        int numberIf = Convert.ToInt32(Console.ReadLine());
                        ParityCheck(numberIf);
                        break;
                    case "8":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите массив чисел через пробел.");
                        string word = Console.ReadLine();                        
                        ArithmeticArray(InputArray(word));
                        break;
                    case "9":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите размер массива");
                        int count = Convert.ToInt32(Console.ReadLine());
                        CreateArrayIndexScuared(count);
                        break;
                    case "10":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите размер массива");
                        int countMas = Convert.ToInt32(Console.ReadLine());
                        CreateArrayEvenNumbers(countMas);
                        break;
                    case "11":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите массив чисел через пробел.");
                        string str = Console.ReadLine();
                        CountEvenNumbers(InputArray(str));
                        break;
                    case "12":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите массив чисел через пробел.");
                        string wordNoSort = Console.ReadLine();
                        SortArray(InputArray(wordNoSort));
                        break;
                    case "13":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите массив чисел через пробел.");
                        string word1 = Console.ReadLine();
                        SortOddNumbers(InputArray(word1));
                        break;
                    case "14":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите массив чисел через пробел.");
                        string word2 = Console.ReadLine();
                        FindDuplicateNumbers(InputArray(word2));
                        break;
                    case "15":
                        Console.Clear();
                        Console.WriteLine(tasks[Convert.ToInt32(numberTask) - 1]);
                        Console.WriteLine("Введите слово.");
                        FindDublicateLetter(Console.ReadLine());
                        break;
                }
                Console.WriteLine("Для выхода из программы нажмите клавишу ESC. Для продолжения нажмите любую другую клавишу.");
                cki = Console.ReadKey();
            }
            while (cki.Key != ConsoleKey.Escape);
        }
        private int[] InputArray(string word)                       //Конвектор строки(числа через пробел) в массив
        {
            string[] mas_string = word.Split(new char[] { ' ' });
            int[] mas = new int[mas_string.Length];
            for (int i = 0; i < mas_string.Length; i++)
            {
                mas[i] = Convert.ToInt32(mas_string[i]);
            }
            return mas;
        }
        private void SumAndMultiplication(int number1, int number2) // вычисление суммы и произведения двух чисел
        {
            Console.WriteLine($"Сумма чисел равна {number1 + number2}. Произведение чисел равно {number1*number2}");
        }
        private void AreaRectangle(float number1, float number2)    //вычисление площади прямоугольника
        {
            Console.WriteLine($"Площадь прямоугольника равна {number1 * number2}");
        }
        private void AreaCircle(float radius)                       //вычисление площади круга по радиусу
        {
            Console.WriteLine($"Площадь круга равна {3.14*(radius*radius)}");
        }
        private void ArithmeticMean(float number1, float number2, float number3)   // вычисление среднего арифметического трех чисел
        {
            float[] array = new[] { number1, number2, number3 };
            float sum = 0;
            foreach(float number in array)
            {
                sum += number;
            }
            Console.WriteLine($"Среднее арифметическое равно {sum / array.Length}");
        }
        private void ProcessingNumber(int number)                   //проверка и обработка заданного числа
        {
            if (number > 10)
                number -= 10;
            else
                number += 10;
            Console.WriteLine(number);
        }
        private void ProcessingNumbers(int number1, int number2)    //проверка и обработка заданных чисел
        {
            if (number1 + number2 > 100)
                Console.WriteLine("yes");
            else
                Console.WriteLine("no");
        }
        private void ParityCheck(int number)                        //проверка на четность
        {
            int a = (number % 2);
            if (a==0)
                Console.WriteLine("yes");
            else
                Console.WriteLine("no");
        }
        private void ArithmeticArray(int[] array1)                  //поиск среднего арифметического чисел массива
        {
            float sum = 0;
            foreach(int number in array1)
            {
                sum += number;
            }
            Console.WriteLine($"Среднее арифметическое элементов массива равно {sum / array1.Length}");
        }
        private void CreateArrayIndexScuared(int countNumbers)      //создание массива, элементы которого равны квадрату своего индекса
        {
            Console.Write($"Массив из {countNumbers} чисел, элементы которого равны квадрату своего индекса: "); ;
            int[] array1 = new int[countNumbers];
            for(int i=0;i<array1.Length;i++)
            {
                array1[i] = i * i;
                Console.Write($"|{array1[i]}");
            }
            Console.WriteLine();
        }
        private void CreateArrayEvenNumbers(int countNumbers)       //создание и заполнение массива четными числами начиная с 2             
        {
            Console.Write("Массив из четных чисел, начиная с 2: ");
            int[] array = new int[countNumbers];
            for(int i=0;i<array.Length;i++)
            {
                if (i == 0)
                    array[i] = 2;
                else
                    array[i] = array[i - 1] + 2;
            }
            foreach (int number in array)
                Console.Write($"|{number}") ;
            Console.WriteLine();
        }
        private void CountEvenNumbers(int[] array)                  //подсчет количества четных элементов в массиве
        {
            int count = 0;
            foreach(int number in array)
            {
                if (number % 2 == 0)
                    count++;
            }
            Console.WriteLine($"В массиве {count} четных элементов.");
        }
        private void SortArray(int[] array)                         //сортировка массива методам пузырька
        {
            Console.Write("Искомый массив: ");
            foreach (int number in array)
                Console.Write($"|{number}");
            Console.WriteLine();
            Sort(ref array);
            Console.Write("Отсортированный массив: ");
            foreach (int number in array)
                Console.Write($"|{number}");
            Console.WriteLine();
        }
        private void Sort(ref int[] array)                          //сортировка массива
        {
            for (int i = 0; i < array.Length - 1; i++)
                for (int j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        int temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
        }
        private void SortOddNumbers(int[] array)                    //поиск и сортировка нечетных чисел 
        {
            Console.Write("Искомый массив: ");
            foreach (int number in array)
                Console.Write($"|{number}");
            Console.WriteLine();
            int countOdd = 0;
            foreach(int number in array)
                if (number % 2 != 0)
                    countOdd++;
            int[] arrayOdd = new int[countOdd];
            int indexArrayOdd = 0;
            for(int i = 0; i < array.Length; i++)
                if (array[i] % 2 != 0)
                {
                    arrayOdd[indexArrayOdd] = array[i];
                    indexArrayOdd++;
                }
            Sort(ref arrayOdd);
            Console.Write("Отсортированный массив нечетных чисел: ");
            foreach (int number in arrayOdd)
                Console.Write($"|{number}");
            Console.WriteLine();
        }
        private void FindDuplicateNumbers(int[] array)              //проверка, есть ли в массиве повторяющиеся числа
        {
            Console.Write("Искомый массив: ");
            foreach (int number in array)
                Console.Write($"|{number}");
            Console.WriteLine();
            for(int i=0;i<array.Length;i++)
                for(int j=0;j<array.Length;j++)
                {
                    if (i == j)
                        break;
                    else if (array[i] == array[j])
                    {
                        Console.WriteLine("В массиве присутствуют повторяющиеся числа!");
                        return;
                    }
                }
            Console.WriteLine("В массиве нет повторяющихся чисел!");
        }
        private void FindDublicateLetter(string word)               //Определение количества повторений каждой буквы в слове
        {
            Console.WriteLine($"Исходная строка: {word}");
            string findLetters = "";
            for (int i = 0; i < word.Length; i++)
            {
                if (findLetters.Contains(word[i]))
                    break;
                else
                    findLetters += word[i];                
            }
            for (int i = 0; i < findLetters.Length; i++)
            {
                int countDublicate = 0;
                for (int j = 0; j < word.Length; j++)
                {
                    if (findLetters[i] == word[j])
                        countDublicate++;
                }
                Console.Write($"букв {findLetters[i]} - {countDublicate} | ");
            }
            
            Console.WriteLine();
        }
    }
}
