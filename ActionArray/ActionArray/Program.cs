﻿using ActionArray.OOP;
using System;

namespace ActionArray
{
    class Program
    {
        static void Main(string[] args)
        {
            FirstAcquaintance dz1 = new();      
            //dz1.Run();                            //запуск выполнения ДЗ(от 8 сентября)

            Gallows game1 = new();                  //запуск игры висельница
           // game1.Start();

            ExploringEasy EE = new();
           // EE.Run();                             //запуск выполнения ДЗ(от 10 сентября)

            Lesson7 lesson7 = new();
            //lesson7.Run();

            SortWordInSentence swis = new();        //запуск выполнения ДЗ(от 15 сентября)
            //swis.Run();


            //Person person1 = new();
            //Console.WriteLine(person1.WritePerson());
            //Person person2 = new("Lexa","Psix","man",22);
            //Console.WriteLine(person2.WritePerson());
            //person2.EditAge(23);
            //person2.EditName("Kari");
            //person2.EditSex("woman");
            //person2.EditSurName("Nama");
            //Console.WriteLine(person2.WritePerson());
            //Console.WriteLine("Массив людей");
            //Person[] people = new Person[5];
            //for(int i = 0; i < 5; i++)
            //{
            //    people[i] = new Person();
            //}
            //foreach(Person p in people)
            //{
            //    Console.WriteLine(p.WritePerson());
            //}


            //Prog prog = new Prog();
            //prog.ProgLesson();
            //Console.ReadKey();

            Figure f1 = new Cube();
            Figure f2 = new Circle(30);
            Figure f3 = new Circle();
            f1.Square();
            f1.Perimeter();
            f2.Square();
            f2.Perimeter();
        }
    }
}
