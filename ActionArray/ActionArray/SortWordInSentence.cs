﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray
{
    public class SortWordInSentence
    {
        public void Run()
        {
            Console.WriteLine("Задание: с консоли вводятся слова через пробел, программа должна вывести эти слова в алфавитном порядке.");
            Console.WriteLine("Введите слова через пробел.");
            string sentence = Console.ReadLine();
            string[] mas_string = sentence.Split(new char[] { ' ' });
            if(DateTime.Now.Second<30)
                Array.Sort(mas_string);                                         //сортировка массива слов с помощью интерфейса IComparable
            else
            {
                bool check = true;
                while (check)
                {
                    check = false;
                    for(int i=0;i<mas_string.Length-1;i++)
                    {
                        if(mas_string[i].CompareTo(mas_string[i+1])>0)
                        {
                            string temp = mas_string[i];
                            mas_string[i] = mas_string[i + 1];
                            mas_string[i + 1] = temp;
                            check = true;
                        }
                    }
                }
            }
                Console.Write("Отсортированная строка: ");
                foreach(string s in mas_string)
                    Console.Write($"{s} ");
            
            Console.ReadKey();
        }
    }
}
