﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionArray
{
    public class Person
    {
        private string Name;
        private string SurName;
        private string Sex;
        private int Age;
        public Person()
        {
            Name = "Ivan";
            SurName = "Kaif";
            Sex = "man";
            Age = 21;
        }
        public Person(string name, string surname, string sex, int age)
        {
            this.Name = name;
            this.SurName = surname;
            this.Sex = sex;
            this.Age = age;
        }
        public void EditName(string newName)
        {
            Name = newName;
        }
        public void EditSurName(string newSurName)
        {
            SurName = newSurName;
        }
        public void EditSex(string newSex)
        {
            Sex = newSex;
        }
        public void EditAge(int newAge)
        {
            Age = newAge;
        }
        public string WritePerson()
        {
            return $"Имя - {Name}, фамилия - {SurName}, пол - {Sex}, лет - {Age}";
        }
    }
}
